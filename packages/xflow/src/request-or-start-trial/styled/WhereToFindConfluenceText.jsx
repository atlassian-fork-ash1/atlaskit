import styled from 'styled-components';

import { colors } from '@atlaskit/theme';

const WhereToFindConfluenceText = styled.p`color: ${colors.N300};`;

WhereToFindConfluenceText.displayName = 'WhereToFindConfluenceText';
export default WhereToFindConfluenceText;
